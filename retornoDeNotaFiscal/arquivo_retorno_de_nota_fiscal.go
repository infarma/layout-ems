package retornoDeNotaFiscal

import (
	"fmt"
	"strconv"
	"time"
)

func HeaderdoArquivo(cnpjDistribuidor string, dataProce time.Time) string {
	headerArquivo := fmt.Sprint("0")
	headerArquivo += fmt.Sprint("NOTA FISCAL OL ")
	headerArquivo += fmt.Sprintf("%015d", ConvertStringToInt(cnpjDistribuidor))
	headerArquivo += fmt.Sprint(dataProce.Format("0201200615040500"))

	return headerArquivo
}

func Header(cnpjFarmacia string, cnpjDistribuidora string, numNotaFiscal int64, numPedidoIndustria string, dataProce time.Time, numPedido int64) string {
	header := fmt.Sprint("1")
	header += fmt.Sprintf("%015d", ConvertStringToInt(cnpjFarmacia))
	header += fmt.Sprintf("%015d", ConvertStringToInt(cnpjDistribuidora))
	header += fmt.Sprintf("%08d", numNotaFiscal)
	header += fmt.Sprintf("%-12d", ConvertStringToInt(numPedidoIndustria))
	header += fmt.Sprint(dataProce.Format("0201200615040500"))
	header += fmt.Sprintf("%012d", numPedido)

	return header
}

func Detalhe(cnpjDistribuidora string, numNotaFiscal int64, codProduto string, numPedido string, condPagamento string, qtdAtendida int32, descAplicado float32, prazoConcedido int32) string {
	detalhe := fmt.Sprint("2")
	detalhe += fmt.Sprintf("%015d", ConvertStringToInt(cnpjDistribuidora))
	detalhe += fmt.Sprintf("%08d", numNotaFiscal)
	detalhe += fmt.Sprintf("%-14d", ConvertStringToInt(codProduto))
	detalhe += fmt.Sprintf("%-12d", ConvertStringToInt(numPedido))
	detalhe += fmt.Sprint(condPagamento)
	detalhe += fmt.Sprintf("%05d", qtdAtendida)
	detalhe += fmt.Sprintf("%03d", int32(descAplicado*100))
	detalhe += fmt.Sprint("00")
	detalhe += fmt.Sprintf("%03d", prazoConcedido)

	return detalhe
}

func Trailer(cnpjDistribuidora string, numNotaFiscal int64, numPedido string, qtdLinhas int32, qtdItens int32) string {
	trailer := fmt.Sprint("3")
	trailer += fmt.Sprintf("%015d", ConvertStringToInt(cnpjDistribuidora))
	trailer += fmt.Sprintf("%08d", numNotaFiscal)
	trailer += fmt.Sprintf("%-12d", ConvertStringToInt(numPedido))
	trailer += fmt.Sprintf("%05d", qtdLinhas)
	trailer += fmt.Sprintf("%05d", qtdItens)

	return trailer
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}