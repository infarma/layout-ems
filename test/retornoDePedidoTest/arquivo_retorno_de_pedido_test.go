package retornoDePedidoTest

import (
	"layout-ems/retornoDePedido"
	"testing"
	"time"
)

func TestHeaderArquivo(t *testing.T){
	cnpjDistribuidor := "24455677000182"
	str := "2017-08-18T18:13:13.001Z"
	dataProce, _ := time.Parse(time.RFC3339, str)

	header := retornoDePedido.HeaderdoArquivo(cnpjDistribuidor, dataProce) //entrar com os dados do banco

	if (len(header) != 47)  {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "0RETORNO PED OL 0244556770001821808201718131300" {
			t.Error("Cabecalho não é compativel")
		}
	}
}

func TestHeader(t *testing.T){
	cnpjFarmacia := "11838929000291"
	numPedIndu := "15713997"
	str := "2017-08-18T18:13:13.001Z"
	dataProce, _ := time.Parse(time.RFC3339, str)
	numPedido := 0
	motivo := 52

	header := retornoDePedido.Header(cnpjFarmacia, numPedIndu, dataProce, int64(numPedido), int32(motivo)) //entrar com os dados do banco

	if (len(header) != 58)  {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "101183892900029115713997    180820171813130000000000000052" {
			t.Error("Cabecalho não é compativel")
		}
	}
}

func TestDetalhe(t *testing.T){
	codEan := 7894916147729
	numPedido := "15713997"
	condPagamento := "0"
	qtdAtendida :=  0
	descApli := 0
	prazoConcedido := 0
	qtdNaoAtendida := 15
	codMotivo := 03
	descMotivo := "Problemas Cadastrais"

	detalhe := retornoDePedido.Detalhe(int64(codEan), numPedido, condPagamento, int32(qtdAtendida), float32(descApli), int32(prazoConcedido), int32(qtdNaoAtendida), int32(codMotivo), descMotivo ) //entrar com os dados do banco

	if (len(detalhe) != 67)  {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if detalhe != "2789491614772915713997    000000000000000001503Problemas Cadastrais" {
			t.Error("Cabecalho não é compativel")
		}
	}
}

func TestTrailerPed(t *testing.T){
	numPedido := "15713997"
	qtdLinhas := 27
	qtdItensAtendidos := 0
	qtdItensNaoAtendidos := 24

	trailer := retornoDePedido.Trailer(numPedido, int32(qtdLinhas), int32(qtdItensAtendidos), int32(qtdItensNaoAtendidos)) //entrar com os dados do banco

	if (len(trailer) != 28)  {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if trailer != "315713997    000270000000024" {
			t.Error("Cabecalho não é compativel")
		}
	}
}