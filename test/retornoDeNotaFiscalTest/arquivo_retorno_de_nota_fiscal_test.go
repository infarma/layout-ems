package retornoDeNotaFiscalTest

import (
	"layout-ems/retornoDeNotaFiscal"
	"testing"
	"time"
)

func TestHeaderArquivo(t *testing.T){
	cnpjDistribuidor := "2000831000199"
	str := "2017-05-08T21:04:52.001Z"
	dataProce, _ := time.Parse(time.RFC3339, str)

	header := retornoDeNotaFiscal.HeaderdoArquivo(cnpjDistribuidor, dataProce) //entrar com os dados do banco

	if (len(header) != 47)  {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "0NOTA FISCAL OL 0020008310001990805201721045200" {
			t.Error("Header do Arquivo não é compativel")
		}
	}
}

func TestHeader(t *testing.T){
	cnpjFarmacia := "958548000653"
	cnpjDistribuidora := "2000831000199"
	numNotaFiscal := 231677
	numPedidoIndustria := "15182078"
	str := "2017-05-08T21:04:52.001Z"
	dataProce, _ := time.Parse(time.RFC3339, str)
	numPedido := 5470544

	header := retornoDeNotaFiscal.Header(cnpjFarmacia, cnpjDistribuidora, int64(numNotaFiscal), numPedidoIndustria, dataProce, int64(numPedido)) //entrar com os dados do banco

	if (len(header) != 79)  {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "10009585480006530020008310001990023167715182078    0805201721045200000005470544" {
			t.Error("Header não é compativel")
		}
	}
}

func TestDetalhe(t *testing.T){
	cnpjDistribuidora := "2000831000199"
	numNotaFiscal := 231677
	codProduto := "7896004715803"
	numPedido := "15182078"
	condPagamento := "0"
	qtdAtendida := 6
	descAplicado := 0.60
	prazoConcedido := 91
	header := retornoDeNotaFiscal.Detalhe(cnpjDistribuidora, int64(numNotaFiscal), codProduto, numPedido, condPagamento, int32(qtdAtendida), float32(descAplicado), int32(prazoConcedido)) //entrar com os dados do banco

	if (len(header) != 64)  {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "2002000831000199002316777896004715803 15182078    00000606000091" {
			t.Error("Detalhe não é compativel")
		}
	}
}

func TestTrailer(t *testing.T){
	cnpjDistribuidora := "2000831000199"
	numNotaFiscal := 231677
	numPedido := "15182078"
	qtdLinhas := 4
	qtdItens := 14

	header := retornoDeNotaFiscal.Trailer(cnpjDistribuidora, int64(numNotaFiscal), numPedido, int32(qtdLinhas), int32(qtdItens)) //entrar com os dados do banco

	if (len(header) != 46)  {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "30020008310001990023167715182078    0000400014" {
			t.Error("Trailer não é compativel")
		}
	}
}
