package arquivoDeEstoqueTest

import (
	"layout-ems/arquivoDeEstoque"
	"testing"
)

func TestDetalhe(t *testing.T){
	codProduto := "7896004715698"
	qtdEstoque := 2724

	detalhe := arquivoDeEstoque.Detalhe(codProduto, int64(qtdEstoque))

	if (len(detalhe) != 24)  {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if detalhe != "807896004715698000002724" {
			t.Error("Cabecalho não é compativel")
		}
	}
}