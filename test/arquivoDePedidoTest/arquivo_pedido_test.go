package arquivoDePedidoTest

import (
	"layout-ems/arquivoDePedido"
	"os"
	"testing"
)

func TestGetArquivoPedido(t *testing.T) {
	f, err := os.Open("../fileTest/PEDems_63400543000388_20191007100032._RM")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	//Header do Arquivo
	var registro arquivoDePedido.HeaderArquivo
	registro.TipoRegistro = 0
	registro.IdentificadorArquivo = "PEDIDO OPER.LOG"
	registro.CnpjDistribuidor = "63400543000388"
	registro.DataProcessamento = 20191007
	registro.HoraProcessamento = 0
	registro.CnpjIndustria = "57507378000365"

	if registro.TipoRegistro != arq.HeaderArquivo.TipoRegistro {
		t.Error("Tipo do registro não é compativel")
	}
	if registro.IdentificadorArquivo != arq.HeaderArquivo.IdentificadorArquivo {
		t.Error("Identificação não é compativel")
	}
	if registro.CnpjDistribuidor != arq.HeaderArquivo.CnpjDistribuidor {
		t.Error("CNPJ da Distribuidora não é compativel")
	}
	if registro.DataProcessamento != arq.HeaderArquivo.DataProcessamento {
		t.Error("Data do Processamento não é compativel")
	}
	if registro.HoraProcessamento != arq.HeaderArquivo.HoraProcessamento {
		t.Error("Hora do Processamento não é compativel")
	}
	if registro.CnpjIndustria != arq.HeaderArquivo.CnpjIndustria {
		t.Error("CNPJ da Industria não é compativel")
	}

	//Header 4 - Cabeçalho do Pedido
	var registro1 arquivoDePedido.HeaderRegistro4
	registro1.TipoRegistro = 1
	registro1.CodigoCliente = 15156220000186
	registro1.NumeroPedido = "18658703"
	registro1.DataPedido = 7102019
	registro1.TipoCompra = "I"
	registro1.TipoRetorno = "C"
	registro1.ApontadorCondicaoComercial = 0
	registro1.NumeroPedidoCliente = ""
	registro1.CodigoRepresentante = 6071

	if registro1.TipoRegistro != arq.HeaderRegistro4.TipoRegistro {
		t.Error("Tipo do registro não é compativel")
	}
	if registro1.CodigoCliente != arq.HeaderRegistro4.CodigoCliente {
		t.Error("Codigo do Cliente não é compativel")
	}
	if registro1.NumeroPedido != arq.HeaderRegistro4.NumeroPedido {
		t.Error("Numero do Pedido não é compativel")
	}
	if registro1.DataPedido != arq.HeaderRegistro4.DataPedido {
		t.Error("Data do Pedido não é compativel")
	}
	if registro1.TipoCompra != arq.HeaderRegistro4.TipoCompra {
		t.Error("Tipo da Compra não é compativel")
	}
	if registro1.TipoRetorno != arq.HeaderRegistro4.TipoRetorno {
		t.Error("Tipo de Retorno não é compativel")
	}
	if registro1.ApontadorCondicaoComercial != arq.HeaderRegistro4.ApontadorCondicaoComercial {
		t.Error("Apontador Condicao Comercial não é compativel")
	}
	if registro1.NumeroPedidoCliente != arq.HeaderRegistro4.NumeroPedidoCliente {
		t.Error("Numero Pedido do Cliente não é compativel")
	}
	if registro1.CodigoRepresentante != arq.HeaderRegistro4.CodigoRepresentante {
		t.Error("Codigo do Representante não é compativel")
	}

	//Condições de Pagamento
	var registro4 arquivoDePedido.CondicaoPagamento
	registro4.TipoRegistro = 4
	registro4.CodigoCondicaoPagamento = 113
	registro4.DescricaoPagamento = "60 DDL"
	registro4.NumeroParcelas = 1
	registro4.PercentualDescontoFinanceiro = 0
	registro4.PrazoPagamentoParcela1 = 60
	registro4.PrazoPagamentoParcela2 = 0
	registro4.PrazoPagamentoParcela3 = 0
	registro4.PrazoPagamentoParcela4 = 0
	registro4.PrazoPagamentoParcela5 = 0
	registro4.PrazoPagamentoParcela6 = 0
	registro4.PercentualPagamentoParcela1 = 100.00
	registro4.PercentualPagamentoParcela2 = 0
	registro4.PercentualPagamentoParcela3 = 0
	registro4.PercentualPagamentoParcela4 = 0
	registro4.PercentualPagamentoParcela5 = 0
	registro4.PercentualPagamentoParcela6 = 0

	if registro4.TipoRegistro != arq.CondicaoPagamento.TipoRegistro {
		t.Error("Tipo do registro não é compativel")
	}
	if registro4.CodigoCondicaoPagamento != arq.CondicaoPagamento.CodigoCondicaoPagamento {
		t.Error("Codigo Condicao de Pagamento não é compativel")
	}
	if registro4.DescricaoPagamento != arq.CondicaoPagamento.DescricaoPagamento {
		t.Error("Descricao Pagamento não é compativel")
	}
	if registro4.PercentualDescontoFinanceiro != arq.CondicaoPagamento.PercentualDescontoFinanceiro {
		t.Error("Percentual Desconto Financeiro não é compativel")
	}
	if registro4.PrazoPagamentoParcela1 != arq.CondicaoPagamento.PrazoPagamentoParcela1 {
		t.Error("Prazo de Pagamento Parcela 1 não é compativel")
	}
	if registro4.PrazoPagamentoParcela2 != arq.CondicaoPagamento.PrazoPagamentoParcela2 {
		t.Error("Prazo de Pagamento Parcela 2 não é compativel")
	}
	if registro4.PrazoPagamentoParcela3 != arq.CondicaoPagamento.PrazoPagamentoParcela3 {
		t.Error("Prazo de Pagamento Parcela 3 não é compativel")
	}
	if registro4.PrazoPagamentoParcela4 != arq.CondicaoPagamento.PrazoPagamentoParcela4 {
		t.Error("Prazo de Pagamento Parcela 4 não é compativel")
	}
	if registro4.PrazoPagamentoParcela5 != arq.CondicaoPagamento.PrazoPagamentoParcela5 {
		t.Error("Prazo de Pagamento Parcela 5 não é compativel")
	}
	if registro4.PrazoPagamentoParcela6 != arq.CondicaoPagamento.PrazoPagamentoParcela6 {
		t.Error("Prazo de Pagamento Parcela 6 não é compativel")
	}
	if registro4.PercentualPagamentoParcela1 != arq.CondicaoPagamento.PercentualPagamentoParcela1 {
		t.Error("Percentual Pagamento Parcela 1 não é compativel")
	}
	if registro4.PercentualPagamentoParcela2 != arq.CondicaoPagamento.PercentualPagamentoParcela2 {
		t.Error("Percentual Pagamento Parcela 2 não é compativel")
	}
	if registro4.PercentualPagamentoParcela3 != arq.CondicaoPagamento.PercentualPagamentoParcela3 {
		t.Error("Percentual Pagamento Parcela 3 não é compativel")
	}
	if registro4.PercentualPagamentoParcela4 != arq.CondicaoPagamento.PercentualPagamentoParcela4 {
		t.Error("Percentual Pagamento Parcela 4 não é compativel")
	}
	if registro4.PercentualPagamentoParcela5 != arq.CondicaoPagamento.PercentualPagamentoParcela5 {
		t.Error("Percentual Pagamento Parcela 5 não é compativel")
	}
	if registro4.PercentualPagamentoParcela6 != arq.CondicaoPagamento.PercentualPagamentoParcela6 {
		t.Error("Percentual Pagamento Parcela 6 não é compativel")
	}

	//Detalhe - Itens do Pedido
	var registro2 arquivoDePedido.ItensPedido

	registro2.TipoRegistro = 2
	registro2.NumeroPedido = "18658703"
	registro2.CodigoProduto = "7896004711553"
	registro2.Quantidade = 3
	registro2.Desconto = 6.9
	registro2.Prazo = 0
	registro2.UtilizacaoDesconto = "Z"
	registro2.UtilizacaoPrazo = "Z"

	if registro2.TipoRegistro != arq.ItensPedido[0].TipoRegistro {
		t.Error("Tipo do Registro não é compativel")
	}
	if registro2.NumeroPedido != arq.ItensPedido[0].NumeroPedido {
		t.Error("Numero do Pedido não é compativel")
	}
	if registro2.CodigoProduto != arq.ItensPedido[0].CodigoProduto {
		t.Error("Codigo do Produto não é compativel")
	}
	if registro2.Quantidade != arq.ItensPedido[0].Quantidade {
		t.Error("Quantidade não é compativel")
	}
	if registro2.Desconto != arq.ItensPedido[0].Desconto {
		t.Error("Desconto não é compativel")
	}
	if registro2.Prazo != arq.ItensPedido[0].Prazo {
		t.Error("Prazo não é compativel")
	}
	if registro2.UtilizacaoDesconto != arq.ItensPedido[0].UtilizacaoDesconto {
		t.Error("Utilizacao Desconto não é compativel")
	}
	if registro2.UtilizacaoPrazo != arq.ItensPedido[0].UtilizacaoPrazo {
		t.Error("Utilizacao Prazo não é compativel")
	}

	//Trailer
	var registro3 arquivoDePedido.Trailler

	registro3.TipoRegistro = 3
	registro3.NumeroPedido = "18658703"
	registro3.QuantidadeItens = 3
	registro3.QuantidadeUnidades = 5

	if registro3.TipoRegistro != arq.Trailler.TipoRegistro {
		t.Error("Tipo do Registro não é compativel")
	}
	if registro3.NumeroPedido != arq.Trailler.NumeroPedido {
		t.Error("Numero do Pedido não é compativel")
	}
	if registro3.QuantidadeItens != arq.Trailler.QuantidadeItens {
		t.Error("Quantidade de Itens não é compativel")
	}
	if registro3.QuantidadeUnidades != arq.Trailler.QuantidadeUnidades {
		t.Error("Quantidade de Unidades não é compativel")
	}
}