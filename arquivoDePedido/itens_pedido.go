package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type ItensPedido struct {
	TipoRegistro       int32   `json:"TipoRegistro"`
	NumeroPedido       string  `json:"NumeroPedido"`
	CodigoProduto      string   `json:"CodigoProduto"`
	Quantidade         int32   `json:"Quantidade"`
	Desconto           float32 `json:"Desconto"`
	Prazo              int32   `json:"Prazo"`
	UtilizacaoDesconto string  `json:"UtilizacaoDesconto"`
	UtilizacaoPrazo    string  `json:"UtilizacaoPrazo"`
}

func (i *ItensPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItensPedido

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&i.NumeroPedido, "NumeroPedido")
	err = posicaoParaValor.ReturnByType(&i.CodigoProduto, "CodigoProduto")
	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	err = posicaoParaValor.ReturnByType(&i.Desconto, "Desconto")
	err = posicaoParaValor.ReturnByType(&i.Prazo, "Prazo")
	err = posicaoParaValor.ReturnByType(&i.UtilizacaoDesconto, "UtilizacaoDesconto")
	err = posicaoParaValor.ReturnByType(&i.UtilizacaoPrazo, "UtilizacaoPrazo")

	i.NumeroPedido = strings.TrimSpace(i.NumeroPedido)

	return err
}

var PosicoesItensPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"NumeroPedido":       {1, 13, 0},
	"CodigoProduto":      {13, 26, 0},
	"Quantidade":         {26, 31, 0},
	"Desconto":           {31, 35, 2},
	"Prazo":              {36, 39, 0},
	"UtilizacaoDesconto": {39, 40, 0},
	"UtilizacaoPrazo":    {40, 41, 0},
}
