package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type Trailler struct {
	TipoRegistro       int32  `json:"TipoRegistro"`
	NumeroPedido       string `json:"NumeroPedido"`
	QuantidadeItens    int32  `json:"QuantidadeItens"`
	QuantidadeUnidades int32  `json:"QuantidadeUnidades"`
}

func (t *Trailler) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailler

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&t.NumeroPedido, "NumeroPedido")
	err = posicaoParaValor.ReturnByType(&t.QuantidadeItens, "QuantidadeItens")
	err = posicaoParaValor.ReturnByType(&t.QuantidadeUnidades, "QuantidadeUnidades")

	t.NumeroPedido = strings.TrimSpace(t.NumeroPedido)

	return err
}

var PosicoesTrailler = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"NumeroPedido":       {1, 13, 0},
	"QuantidadeItens":    {13, 18, 0},
	"QuantidadeUnidades": {18, 28, 0},
}
