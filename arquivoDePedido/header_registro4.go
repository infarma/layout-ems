package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type HeaderRegistro4 struct {
	TipoRegistro               int32  `json:"TipoRegistro"`
	CodigoCliente              int64  `json:"CodigoCliente"`
	NumeroPedido               string `json:"NumeroPedido"`
	DataPedido                 int32  `json:"DataPedido"`
	TipoCompra                 string `json:"TipoCompra"`
	TipoRetorno                string `json:"TipoRetorno"`
	ApontadorCondicaoComercial int32  `json:"ApontadorCondicaoComercial"`
	NumeroPedidoCliente        string `json:"NumeroPedidoCliente"`
	CodigoRepresentante        int32  `json:"CodigoRepresentante"`
}

func (h *HeaderRegistro4) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeaderRegistro4

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&h.CodigoCliente, "CodigoCliente")
	err = posicaoParaValor.ReturnByType(&h.NumeroPedido, "NumeroPedido")
	err = posicaoParaValor.ReturnByType(&h.DataPedido, "DataPedido")
	err = posicaoParaValor.ReturnByType(&h.TipoCompra, "TipoCompra")
	err = posicaoParaValor.ReturnByType(&h.TipoRetorno, "TipoRetorno")
	err = posicaoParaValor.ReturnByType(&h.ApontadorCondicaoComercial, "ApontadorCondicaoComercial")
	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoCliente, "NumeroPedidoCliente")
	err = posicaoParaValor.ReturnByType(&h.CodigoRepresentante, "CodigoRepresentante")

	h.NumeroPedido = strings.TrimSpace(h.NumeroPedido)
	h.NumeroPedidoCliente = strings.TrimSpace(h.NumeroPedidoCliente)

	return err
}

var PosicoesHeaderRegistro4 = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":               {0, 1, 0},
	"CodigoCliente":              {1, 16, 0},
	"NumeroPedido":               {16, 28, 0},
	"DataPedido":                 {28, 36, 0},
	"TipoCompra":                 {36, 37, 0},
	"TipoRetorno":                {37, 38, 0},
	"ApontadorCondicaoComercial": {38, 43, 0},
	"NumeroPedidoCliente":        {43, 58, 0},
	"CodigoRepresentante":        {58, 67, 0},
}
