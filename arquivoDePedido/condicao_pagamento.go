package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type CondicaoPagamento struct {
	TipoRegistro                 int32   `json:"TipoRegistro"`
	CodigoCondicaoPagamento      int32   `json:"CodigoCondicaoPagamento"`
	DescricaoPagamento           string  `json:"DescricaoPagamento"`
	NumeroParcelas               int32   `json:"NumeroParcelas"`
	PercentualDescontoFinanceiro float32 `json:"PercentualDescontoFinanceiro"`
	PrazoPagamentoParcela1       int32   `json:"PrazoPagamentoParcela1"`
	PrazoPagamentoParcela2       int32   `json:"PrazoPagamentoParcela2"`
	PrazoPagamentoParcela3       int32   `json:"PrazoPagamentoParcela3"`
	PrazoPagamentoParcela4       int32   `json:"PrazoPagamentoParcela4"`
	PrazoPagamentoParcela5       int32   `json:"PrazoPagamentoParcela5"`
	PrazoPagamentoParcela6       int32   `json:"PrazoPagamentoParcela6"`
	PercentualPagamentoParcela1  float32 `json:"PercentualPagamentoParcela1"`
	PercentualPagamentoParcela2  float32 `json:"PercentualPagamentoParcela2"`
	PercentualPagamentoParcela3  float32 `json:"PercentualPagamentoParcela3"`
	PercentualPagamentoParcela4  float32 `json:"PercentualPagamentoParcela4"`
	PercentualPagamentoParcela5  float32 `json:"PercentualPagamentoParcela5"`
	PercentualPagamentoParcela6  float32 `json:"PercentualPagamentoParcela6"`
}

func (c *CondicaoPagamento) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCondicaoPagamento

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&c.CodigoCondicaoPagamento, "CodigoCondicaoPagamento")
	err = posicaoParaValor.ReturnByType(&c.DescricaoPagamento, "DescricaoPagamento")
	err = posicaoParaValor.ReturnByType(&c.NumeroParcelas, "NumeroParcelas")
	err = posicaoParaValor.ReturnByType(&c.PercentualDescontoFinanceiro, "PercentualDescontoFinanceiro")
	err = posicaoParaValor.ReturnByType(&c.PrazoPagamentoParcela1, "PrazoPagamentoParcela1")
	err = posicaoParaValor.ReturnByType(&c.PrazoPagamentoParcela2, "PrazoPagamentoParcela2")
	err = posicaoParaValor.ReturnByType(&c.PrazoPagamentoParcela3, "PrazoPagamentoParcela3")
	err = posicaoParaValor.ReturnByType(&c.PrazoPagamentoParcela4, "PrazoPagamentoParcela4")
	err = posicaoParaValor.ReturnByType(&c.PrazoPagamentoParcela5, "PrazoPagamentoParcela5")
	err = posicaoParaValor.ReturnByType(&c.PrazoPagamentoParcela6, "PrazoPagamentoParcela6")
	err = posicaoParaValor.ReturnByType(&c.PercentualPagamentoParcela1, "PercentualPagamentoParcela1")
	err = posicaoParaValor.ReturnByType(&c.PercentualPagamentoParcela2, "PercentualPagamentoParcela2")
	err = posicaoParaValor.ReturnByType(&c.PercentualPagamentoParcela3, "PercentualPagamentoParcela3")
	err = posicaoParaValor.ReturnByType(&c.PercentualPagamentoParcela4, "PercentualPagamentoParcela4")
	err = posicaoParaValor.ReturnByType(&c.PercentualPagamentoParcela5, "PercentualPagamentoParcela5")
	err = posicaoParaValor.ReturnByType(&c.PercentualPagamentoParcela6, "PercentualPagamentoParcela6")

	c.DescricaoPagamento = strings.TrimSpace(c.DescricaoPagamento)

	return err
}

var PosicoesCondicaoPagamento = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                 {0, 1, 0},
	"CodigoCondicaoPagamento":      {1, 6, 0},
	"DescricaoPagamento":           {6, 36, 0},
	"NumeroParcelas":               {36, 39, 0},
	"PercentualDescontoFinanceiro": {39, 44, 2},
	"PrazoPagamentoParcela1":       {44, 47, 0},
	"PrazoPagamentoParcela2":       {47, 50, 0},
	"PrazoPagamentoParcela3":       {50, 53, 0},
	"PrazoPagamentoParcela4":       {53, 56, 0},
	"PrazoPagamentoParcela5":       {56, 59, 0},
	"PrazoPagamentoParcela6":       {59, 62, 0},
	"PercentualPagamentoParcela1":  {62, 67, 2},
	"PercentualPagamentoParcela2":  {67, 72, 2},
	"PercentualPagamentoParcela3":  {72, 77, 2},
	"PercentualPagamentoParcela4":  {77, 82, 2},
	"PercentualPagamentoParcela5":  {82, 87, 2},
	"PercentualPagamentoParcela6":  {87, 92, 2},
}
