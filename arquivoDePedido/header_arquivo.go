package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type HeaderArquivo struct {
	TipoRegistro         int32  `json:"TipoRegistro"`
	IdentificadorArquivo string `json:"IdentificadorArquivo"`
	CnpjDistribuidor     string  `json:"CnpjDistribuidor"`
	DataProcessamento    int32  `json:"DataProcessamento"`
	HoraProcessamento    int32  `json:"HoraProcessamento"`
	CnpjIndustria        string  `json:"CnpjIndustria"`
}

func (h *HeaderArquivo) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeaderArquivo

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&h.IdentificadorArquivo, "IdentificadorArquivo")
	err = posicaoParaValor.ReturnByType(&h.CnpjDistribuidor, "CnpjDistribuidor")
	err = posicaoParaValor.ReturnByType(&h.DataProcessamento, "DataProcessamento")
	err = posicaoParaValor.ReturnByType(&h.HoraProcessamento, "HoraProcessamento")
	err = posicaoParaValor.ReturnByType(&h.CnpjIndustria,"CnpjIndustria")

	h.CnpjDistribuidor = strings.TrimSpace(h.CnpjDistribuidor)
	h.CnpjIndustria = strings.TrimSpace(h.CnpjIndustria)

	return err
}

var PosicoesHeaderArquivo = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":         {0, 1, 0},
	"IdentificadorArquivo": {1, 16, 0},
	"CnpjDistribuidor":     {16, 31, 0},
	"DataProcessamento":    {31, 39, 0},
	"HoraProcessamento":    {39, 47, 0},
	"CnpjIndustria":        {47, 62, 0},
}
