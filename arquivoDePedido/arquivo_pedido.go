package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	HeaderArquivo     HeaderArquivo     `json:"HeaderArquivo"`
	Header            Header            `json:"Header"`
	HeaderRegistro4   HeaderRegistro4   `json:"HeaderRegistro4"`
	CondicaoPagamento CondicaoPagamento `json:"CondicaoPagamento"`
	ItensPedido       []ItensPedido     `json:"ItensPedido"`
	Trailler          Trailler          `json:"Trailler"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)
	//fileScanner := fileScanner2
	arquivo := ArquivoDePedido{}
	var err error
	//registro4 := false

	//for fileScanner2.Scan() {
	//	runes2 := []rune(fileScanner2.Text())
	//	ident := string(runes2[0:1])
	//	if ident == "4"{
	//		registro4 = true
	//		break
	//	}
	//}

	for fileScanner.Scan() {

		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		if identificador == "0" {
			err = arquivo.HeaderArquivo.ComposeStruct(string(runes))
		} else if identificador == "1" {
			//if registro4 {
			//
			//} else{
			//	err = arquivo.Header.ComposeStruct(string(runes))
			//}
			err = arquivo.HeaderRegistro4.ComposeStruct(string(runes))
		} else if identificador == "4" {
			err = arquivo.CondicaoPagamento.ComposeStruct(string(runes))
		} else if identificador == "2" {
			itemPedido := ItensPedido{}
			itemPedido.ComposeStruct(string(runes))
			arquivo.ItensPedido = append(arquivo.ItensPedido, itemPedido)
		} else if identificador == "3" {
			err = arquivo.Trailler.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
