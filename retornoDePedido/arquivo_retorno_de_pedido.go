package retornoDePedido

import (
	"fmt"
	"strconv"
	"time"
)

func HeaderdoArquivo(cnpjDistribuidor string, dataProce time.Time) string {
	headerArquivo := fmt.Sprint("0")
	headerArquivo += fmt.Sprint("RETORNO PED OL ")
	headerArquivo += fmt.Sprintf("%015d", ConvertStringToInt(cnpjDistribuidor))
	headerArquivo += fmt.Sprint(dataProce.Format("0201200615040500"))

	return headerArquivo
}

func Header(cnpjFarmacia string, numPedIndu string, dataProce time.Time, numPedido int64, motivo int32) string {
	header := fmt.Sprint("1")
	header += fmt.Sprintf("%015d", ConvertStringToInt(cnpjFarmacia))
	header += fmt.Sprintf("%-12d", ConvertStringToInt(numPedIndu))
	header += fmt.Sprint(dataProce.Format("0201200615040500"))
	header += fmt.Sprintf("%012d", numPedido)
	header += fmt.Sprintf("%02d", motivo)

	return header
}

func Detalhe(codEan int64, numPedido string, condPagamento string, qtdAtendida int32, descApli float32, prazoConcedido int32,
							qtdNaoAtendida int32, codMotivo int32, descMotivo string) string {
	detalhe := fmt.Sprint("2")
	detalhe += fmt.Sprintf("%013d", codEan)
	detalhe += fmt.Sprintf("%-12d", ConvertStringToInt(numPedido))
	detalhe += fmt.Sprintf("%-1d", ConvertStringToInt(condPagamento))
	detalhe += fmt.Sprintf("%05d", qtdAtendida)
	detalhe += fmt.Sprintf("%03d", int32(descApli))
	detalhe += fmt.Sprintf("%05d", prazoConcedido)
	detalhe += fmt.Sprintf("%05d", qtdNaoAtendida)
	detalhe += fmt.Sprintf("%02d", codMotivo)
	detalhe += fmt.Sprint(descMotivo)
	return detalhe
}

func Trailer(numPedido string, qtdLinhas int32, qtdItensAtendidos int32, qtdItensNaoAtendidos int32) string {
	trailer := fmt.Sprint("3")
	trailer += fmt.Sprintf("%-12d", ConvertStringToInt(numPedido))
	trailer += fmt.Sprintf("%05d", qtdLinhas)
	trailer += fmt.Sprintf("%05d", qtdItensAtendidos)
	trailer += fmt.Sprintf("%05d", qtdItensNaoAtendidos)

	return trailer
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}