package arquivoDeEstoque

import (
	"fmt"
	"strconv"
)

func Detalhe(codProduto string, qtdEstoque int64) string {
	detalhe := fmt.Sprint("80")
	detalhe += fmt.Sprintf("%013d", ConvertStringToInt(codProduto))
	detalhe += fmt.Sprintf("%09d", qtdEstoque)
	return detalhe
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}